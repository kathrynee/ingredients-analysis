
# general stats on the corpus

## the total number of chunks by year

 ```sql
 select
   year,
   count(*) as chunks
from  chunks c
join  documents d on (c.document_id = d.id)
group by year
order by year;
 ```

| year | chunks
|------|-------
| 1997 |  25145
| 2016 |  23347

## Total number of chunks with chunk confidence greater than 0.5 by year

 ```sql
 select
   year,
   count(*) as valid_chunks
from  chunks c
join  documents d on (c.document_id = d.id)
group by year
order by year;
 ```

| year | chunks
|------|-------
| 1997 |   3759
| 2016 |   4378

## Total number of chunks with chunk confidence > 0.5 and term confidence > 0.85 by year

 ```sql
with blah as (select chunk_id from valid_terms t join valid_chunks vc on (t.chunk_id = vc.id) group by chunk_id)
select
  year,
  count(*)
from  blah b
join  chunks c on (b.chunk_id = c.id)
join  documents d on (c.document_id = d.id)
group by year
order by year;
```

| year | count
|------|------
| 1997 |  2083
| 2016 |  2692

# Total number of times oil and butter are mentioned

```sql
select year, count(*)
from   terms t
join   chunks c on (t.chunk_id = c.id)
join   documents d on (c.document_id = d.id)
where  term ilike 'oil' or term ilike 'butter'
group by year
order by year;
```

| year | count
|------|------
| 1997 |   815
| 2016 |  1168


# Total number of times oil and butter are mentioned in the 0.5/0.8 cut off

```sql
select year, count(*)
from   ingredients i
join   chunks c on (i.chunk_id = c.id)
join   documents d on (c.document_id = d.id)
where  term ilike 'oil' or term ilike 'butter'
group by year;
```

| year | count
|------|------
| 1997 |   183
| 2016 |   263


# Random facts about butter and margarine

```sql
select count(*), year from (select year, document_id, count(*) from terms t join chunks c on (t.chunk_id = c.id) join documents d on (d.id = c.document_id) where t.term ilike '%margarine%' group by year, document_id) as t group by year order by year;
```

| count | year
|-------|------
|     5 | 1997
|     6 | 2016


```sql
select count(*), year from (select year, document_id, count(*) from terms t join chunks c on (t.chunk_id = c.id) join documents d on (d.id = c.document_id) where t.term ilike '%butter%' group by year, document_id) as t group by year order by year;
```

| count | year
|-------|------
|   105 | 1997
|   182 | 2016



can you give me any stats on the QTY and UNIT which goes with butter and oil in the chunks? Most frequently used QTY and UNIT? Or the range of QTY/UNITS?



```sql
with ingredients_with_quantity as (select chunk_id, term as qty from ingredients where tag = 'B-QTY' and confidence > 0.85)
select *
from   ingredients_with_quantity i
join   chunks c on (i.chunk_id = c.id)
join   documents d on (c.document_id = d.id);
```


# Create
