= FINAL RESULTS 


=  General stats on the corpus

== Total number of chunks

=== Output

| Year | Count
|------|-------
| 1997 | 25145
| 2016 | 23347


=== SQL:
```sql
select year, count(*) from chunks join chunks_by_year using (id) group by year order by year;
```

== Total number of chucks above the 0.5/0.85 threshold

=== Output

| Year | Count
|------|-------
| 1997 |  2083
| 2016 |  2692


=== SQL
```sql
with valid_ingredients as (select chunk_id from valid_terms t join valid_chunks vc on (t.chunk_id = vc.id) group by chunk_id),
     valid_chunk_ids  as (select t.chunk_id from terms t join valid_ingredients b on (t.chunk_id = b.chunk_id) group by t.chunk_id)
select   year, count(chunk_id)
from     valid_chunk_ids join chunks_by_year cy on (valid_chunk_ids.chunk_id = cy.id)
group by year
order by year;
```

= Oil and Butter stats

== Total number of chunks
=== Output

=== SQL
```sql
```
== Total number of chucks above the 0.5/0.85 threshold
=== Output
| Year | Count
|------|-------
| 1997 |   130
| 2016 |   179


=== SQL
```sql
select year, count(*) from chunks_by_category join chunks_by_year using (id) where category = 'fat' group by year order by year;
```
== Average quantity of butter or oil per chunk
=== Output

NOTE: Use these results

| Year | Average Quantity |  SD
|------|------------------|------
| 1997 |    35.09g        | 3.02%
| 2016 |    67.22g        | 6.53%


IMPORTANT: INCORRECT — original value (tbsp == 30ml) 

| Year | Chuck
|------|-------------
| 1997 | 50.22g
| 2016 | 75.76g

=== SQL
```sql
select   year as "Year", round(avg(abs_qty * 1000)::numeric, 2) as "Grams/chuck"
from     results r
join     chunks_by_category c on (r.chunk_id = c.id)
join     chunks_by_year y on (c.id = y.id)
where    c.category = 'fat'
group by year
order by year;
```

== Average quantity of butter or oil per chunk by butter/oil
=== Output

NOTE: Use these results

| Year   | 1997   | 2016   | Change
|--------|--------|--------|-------
| Oil    | 43.53g | 92.47g | +112%
| Butter | 18.58g | 52.83g | +184%


IMPORTANT: INCORRECT — original value (tbsp == 30ml) 

| Year   | 1997   | 2016  | Change
|--------|--------|-------|-------
| Oil    | 64.15g  | 65.58g | +2.2%
| Butter | 22.99g  | 93.61g | +307.2%


=== SQL
```sql
with r as (select chunk_id, abs_qty, case when name = 'butter' then 'butter' when name = 'unsalted' then 'butter' else 'oil' end as cat from results)
select   year as "Year", cat, round(avg(abs_qty * 1000)::numeric, 2) as "Grams/chuck"
from     r
join     chunks_by_category c on (r.chunk_id = c.id)
join     chunks_by_year y on (c.id = y.id)
where    c.category = 'fat'
group by year, cat
order by year, cat;
```


== list of the most frequently used oil
=== Output

==== 1997
| name      | count
|-----------|-------
| olive     |    67
| butter    |    44
| peanut    |     8
| sesame    |     7
| oil       |     2
| grapeseed |     1
| tomato    |     1


==== 2016
| name      | count
|-----------|-------
| olive     |    86
| butter    |    65
| sesame    |    12
| grapeseed |     6
| peanut    |     4
| salted    |     2
| almond    |     1
| canola    |     1
| oil       |     1
| sunflower |     1


=== SQL
```sql
with results_butter as (select chunk_id, case when name = 'butter' then 'butter' when name = 'unsalted' then 'butter' else name end from results)
select year, name, count(r.name)
from results_butter r  
join chunks_by_category c on (r.chunk_id = c.id)
join chunks_by_year y on (c.id = y.id)
where category = 'fat'
group by year, name
order by year, count desc, name;
```

