#PICO Script
<!--
  The proof of concept presentation was a PICO presentation, as per the European Geosciences Union
  General Assembly 2014. In this format the session starts with a 2-minute-madness presentation by each
  participant. After this, longer versions of each presentation are available on screens for discussion 
  between the audience and presenter. Details are here http://www.egu2014.eu/pico.html
  This is my script for the initial 2-minute presentation.
-->

|| Greeting

|| Introduce myself

Public concern about obesity has fostered intense debate about the causes of widespread population weight gain.

One area which has received both academic _and_ media attention, is increasing food portion sizes. 

(Slide) Have our meals changed from this to this?

Increased portion sizes have been documented in fast food menus. However, there is little known about the role of published recipes in changing portion size perception.

This semester I have been working on a method of finding and analysing the ingredients used in newspaper recipes.

I know from experience that manually collecting, reading and analysing newspaper articles is a laborious and often tedious process. During the Qualitative Research Methods course it took me a day per article.

For my proof of concept, I have downloaded 1,500 articles from the Sydney Morning Herald archives. 

(Slide) I then used machine learning to find recipes and ingredient lists in these articles.

The machine learning technique I have used, teaches your computer to recognise words and semantic structures.

With machine learning, instead of a day per article, it takes my old, heavy and unglamorous computer about 10 minutes to read 1,500 articles and identify the recipes.

For my proof of concept I show how recipe ingredients can be quickly identified and extracted from two years of newspaper articles. These ingredients can then be compared and analysed, to find out if there has been a change in published recipe portions. For a Masters project, even more data could be used.
