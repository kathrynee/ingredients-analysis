Copyright 2017 Kathryn Elliott

Ingredients Analysis by Kathryn Elliott is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. Based on a work at https://gitlab.com/kathrynee/ingredients-analysis.

You may obtain a copy of the License at: https://creativecommons.org/licenses/by-sa/4.0/
