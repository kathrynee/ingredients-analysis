# Database

The output of the NYT ingredient parser and part of FSANZ database was added to
a Postgres database, cleaned up and finally SQL queries were run over this data
to get the results.

For the sake of this document I will assume the database is called `ingredients`.
If you want to use a different name set and export the PGDATABASE environment
variable to the name of the database.

Oh and one last thing the shell is `zsh`. So if you run it in `bash` it might not
work as expected. It should be easy to change though.


# Importing data

## Create the database and schema

Create the database by running:

```bash
createdb ingredients
```

and then create the schema:

```bash
cat schema.sql | psql ingredients
```

If you want to use a different database name set (and export) the environment
variable `PGDATABASE` to be whatever the database name you want to use and
simply remove the word "ingredients" in the above commands. For example:

```bash
export PGDATABASE=foobar
createdb
cat schema.sql | psql
```

The `load-db` script will also use `PGDATABASE`.

I'm going assume that $PGDATABASE is set for the remainder of this document.

## Running the Import

The main script is called `load-db` which bludgeons the data into the correct
format and uses a combination of sql and Postgres' data import (`\copy`) to
insert the data.

To run `load-db` over all parsed documents:

```bash
echo <documents dir>/*.tagged | xargs -n1 bin/load-db 2>&1 | tee import.log
```

Given the somewhat hacky nature of these scripts If you need to rerun it you
will need to drop the database and re-create it.

```bash
dropdb ingredients && createdb ingredients
cat schema.sql | psql
```

WARNING: You could also use PGDATABASE and not add the database name when dropping &
creating the database but if you get it wrong it will remove the default
database (named after $LOGNAME).


## Running the ETL process.

Once you are happy that the basic import has worked you need to run the ETL SQL
script which will massage the data into some sort of normalised form:

```bash
cat sql/etl.sql | psql
```

## Import the FSANZ data

```bash
echo "\\\copy fsanz from" =(bin/parse-fsanz fsanz.tsb) | psql
```

## Data Cleaning

```bash
cat sql/data-cleanup.sql | psql
```

And that is pretty much it. The above instructions should Just Work™ but you
might need to manually intervene if there are problems.


# Reflection

The cleanup stage, instead of being part of the ETL as it should have been,
ended up taking on a life of it's own making the database much messier than it
should be I didn't clean this up due to time constraints.

This process was messy and ad hoc. In hindsight I would have used a language to
process the data and added it to the database simply to produce the final
stats.
