# Digital humanities proof of concept: A way of studying portion changes in published recipes

## Background

This project was put together as part of the FOAR705 -- Digital Humanities --
Masters of Research course at Macquarie University (Sydney, Australia). It is a
proof of concept, attempting to use machine learning techniques to find and
extract recipe ingredients from a large number of newspaper articles.

The original corpus of documents was downloaded from ProQuest
(http://www.proquest.com/) through Macquarie University's Library access
rights. Due to copyright reasons, I have not included the corpus in this
repository. The original corpus contained articles from _The Sydney Morning
Herald_ newspaper in 1997 and 2016.

The _New York Times_ ingredients parser was used to identify recipe ingredients
from my article corpus. This parser was released by _The New York Times_ in
2016 under an Apache 2.0 open source license. The announcement of the release
is here: [Data Our Tagged Ingredients Data is Now on
GitHub](https://open.blogs.nytimes.com/2016/04/27/structured-ingredients-data-tagging/?_r=0)
and the GitHub repository for the ingredients parser is here:
https://github.com/NYTimes/ingredient-phrase-tagger

## Outline of the project

For this proof of concept, I searched the ProQuest archive for _Sydney Morning 
Herald_ articles, using the search term "recipe OR ingredient" for the years 
1997 and 2016, which resulted in 1,500 articles. These were downloaded and 
saved to my file system. 

I then used `bash` to:

* Separate the ProQuest download results into individual files
* Extract metadata from each article
* Translate machine learning training set from US to Australian terms
* Reduce repetitive work: save on typing!

The articles were then run through the NYT ingredients-parser to identify 
recipe ingredients within the corpus.

The results of this were then placed in a Postgres database, for analysis of
changing ingredient usage over time. As this was a proof of concept only, I 
focused on one food group - fats and oils. 

For a full overview of the project, see my `.pdf` presentation in the 
`/presentation` directory.

## separate-text

This an ``awk`` script, matching text between two points, extracting that text
and then saving it to a separate file. I downloaded my data corpus in two
batches, one had 1,000 documents and the second 522 documents. The
``separate-text`` script extracts each individual article from these two
batches. Within the download, each article started with a specific format of
document numbering (Document 1 of 1000 or Document 1 of 522, etc) and ended
with a long line of hyphens. Hence I've used these for the start and end
markers of my script. These markers would need to be amended for other uses.

I have started all my scripts with the following with a usage message. This 
means that if you use the script with no arguments, it will fail. However, 
instead of just silently failing, this script prints a message about how to 
use the script.

## extract-metadata

Another ``awk`` script, this time to separate the metadata from the downloaded
articles. One of the benefits of the ProQuest download was that the article
text and metadata were clearly defined, meaning each article could be searched
for certain terms and the text between those matching points extracted. The
``extract-metadata`` script creates two files.

Firstly, the _content_ of each article, which is matched between the two points
of "Full Text" and "Title" and saved with an additional `.content` suffix. The
script also removes the matching words "Full Text" and "Title" before saving.

Secondly, the _metadata_, matched on a list of fields and then saved as a
`.metadata` file.

## au-converter

As stated above, the original ingredients parser was developed by _The New York
Times_ and included a training corpus of about 180,000 ingredient lines. This
corpus was from the NYT recipe archives and therefore featured US measurements
and ingredient names. The ``au-converter`` script was written to translate the
parser into Australian terminology. 

It is a ``sed`` script which searches a file (in this instance the training
corpus) for a number of words and then converts them into different terms. I
compiled the list largely from my own knowledge of writing recipes for both
Australian and international audiences. I backed that up, with some Google
research and I also looked through the training corpus to spot problemmatic
terms.

## Postgres database

The results were put into a Postgres database, for cleaning and analysis. As
this was not part of the original scope of my project, the database work was
done by my partner. Various methods were used to clean the corpus and refine
the dataset to ensure I was just analysing recipe ingredients, rather than the
rest of the text. An explanation of these cleaning methods are included in the
``data-cleanup.sql`` file.

_Note for my unit convener:_ The data cleaning stage was very fragile, due 
to its ad-hoc implementation. If you want to run the SQL queries, it might 
best to import the database dump I've uploaded to Google Drive and run the 
queries there.

## Basic statistics analysis

A number of statistics were generated from the corpus, including the total
number of chunks which were recipe ingredients, the number of times oil and
butter were mentioned in the corpus, the average quantities used and so on. A
list of the final Postgres queries used and the results obtained are in the
``/doc`` directory.
