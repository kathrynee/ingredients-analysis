#!/bin/bash
# An `awk` script to separate out individual articles from my larger corpus
# Within my corpus each article started with a specific format of document 
# numbering (Document 1 of 1000 or Document 1 of 522, etc) and ended with a 
# long line of hyphens. I've used these for the start and end markers of my 
# script. These markers would need to be amended for other uses.


# I'd love to claim this crazy line as all my own work! But it wasn't and I had
# some help with the syntax.
title() {
	cat $1 | grep '^Title: ' | sed "s/^Title: //; s/\([^;:]*\)[;:].*\$/\1/g; s/ /-/g; s/]//; s/\[//g; s/[?!,\.\"']//g" | tr '[[:upper:]]' '[[:lower:]]'
}

year() {
  cat $1 | grep '^Publication year: ' | sed 's/^Publication year: //'
}


input_file=$1

if [[ -z $input_file ]]; then
	echo "usage: $0 <input file>"
	exit 1
fi

output_dir=$(basename $input_file | sed 's/\.txt$//')

mkdir -p $output_dir

# First batch - comment out if using second batch option
number_of_documents=1000
offset=0

# Second batch - comment out if using first batch option
number_of_documents=522
offset=1000

end_marker="____________________________________________________________"

for i in $(seq 1 $number_of_documents); do
    
  tmp_file=/tmp/$i
  
	start_marker="Document $i of $number_of_documents"
	
	cat $input_file | awk "/$start_marker/, /$end_marker/ {print \$0}" | grep -v $end_marker > $tmp_file
	
  year=$(year $tmp_file)
  title=$(title $tmp_file)
  
  output_filename=$year-$(($i + $offset))-$title.txt
  
  echo "Output document: $output_filename"
  
  grep -v "$start_marker" $tmp_file > $output_dir/$output_filename
done
