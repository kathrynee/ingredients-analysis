-- This is all very messy and horrible and, in hindsight, it would have been **far**
-- better to do the preliminary work in ruby or $SCRIPTING_LANGUAGE_OF_CHOICE.

-- Please do not judge me for this atrocity! --- rgh

-- Remove all the results tables.
drop table if exists results_no_bad_qty, results, qty_normalisation, unit_normalisation;
drop view if exists results_with_duplicates;


create view results_with_duplicates as
select c.chunk_id, name, sub_name, unit, qty from (select chunk_id from terms group by chunk_id) as c
left outer join (select chunk_id, term as name from terms where tag = 'B-NAME') as n using (chunk_id)
left outer join (select chunk_id, term as sub_name from terms where tag = 'I-NAME') as s using (chunk_id)
left outer join (select chunk_id, term as unit from terms where tag = 'B-UNIT') as u using (chunk_id)
left outer join (select chunk_id, term as qty from terms where tag = 'B-QTY') as q using (chunk_id);


-- Remove records where there are multiple sub_names; they are mostly wrong anyway.
create table results as
select * from results_with_duplicates where chunk_id in (select chunk_id from results_with_duplicates group by chunk_id, name having count(*) = 1);


-- Fix the majority of unit and quantities where the parser hasn't parsed 
-- the unit/quanties, eg where there is no space between qty and unit (ml, g & tbs)
create table results_no_bad_qty as
select * from results where qty ~ '[a-z]$' and qty not like 'extra%' and qty not like 'par%' and qty not like 'buffalo%' and unit is null;

update results set (unit, qty) =
  (select regexp_replace(qty, '[0-9][0-9]*', '') as unit, regexp_replace(qty, '[a-z][a-z]*$', '') as qty from results_no_bad_qty
   where chunk_id = results.chunk_id)
  where results.chunk_id in (select chunk_id from results_no_bad_qty);


-- Fix the incorrect (but obvious) values and convert rational numbers to reals.
alter table results add normalised_qty text;

create table qty_normalisation (qty text, value text);
insert into qty_normalisation values ('1/2', 0.5);
insert into qty_normalisation values ('1$1/2', 0.5);
insert into qty_normalisation values ('2$1/2', 0.5);
insert into qty_normalisation values ('3/4', 0.75);
insert into qty_normalisation values ('1/4', 0.25);


-- Note these conversion factors convert from unit to litre.
create table unit_normalisation (unit text, conversion_factor float);
insert into unit_normalisation values ('tbs', 0.02);
insert into unit_normalisation values ('cup', 0.25);
insert into unit_normalisation values ('grams', 0.001);
insert into unit_normalisation values ('tsp', 0.005);
insert into unit_normalisation values ('tbsp', 0.02);
insert into unit_normalisation values ('cups', 0.25);
insert into unit_normalisation values ('g', 0.001);
insert into unit_normalisation values ('ml', 0.001);


-- Convert the single instance where '1' was actually an 'l' (I'm assuming it's a 1 anyway). Homoglyphs are awesome!
update results set qty = 1 where qty = 'l';


-- Remove rows that have two entries in the results table. This is because of alternation in the ingredients
delete from results where chunk_id = 42266 and name = 'peanuts';
delete from results where chunk_id = 25007 and name = 'olive';


-- Normalise the rational numbers. I'm sure there's a better way than this!
with foo as (select chunk_id, value as qty from results join qty_normalisation using (qty))
update results set (qty) = (select qty from foo where chunk_id = results.chunk_id)
where chunk_id in (select chunk_id from foo);


-- Apply a conversion to each value to normalise mass to kgs and volume to litres.
with foo as (select chunk_id, qty::float * conversion_factor as normalised_qty from results join unit_normalisation using (unit) where unit is not null and qty is not null)
update results set normalised_qty = (select normalised_qty from foo where chunk_id = results.chunk_id)
where chunk_id in (select chunk_id from foo);


-- Normalise grams to g
update results set unit = 'gram' where unit = 'g';


-- Create a new column for the absolute quantity.
alter table results add abs_qty float;


-- The conversion factors were hand extracted from the USDA database and take into
-- account that volume measurements are different in the US.

update results set abs_qty = round(normalised_qty::numeric, 4) where unit = 'gram';

update results set abs_qty = round(normalised_qty::numeric * (16 / 14.8), 4) where name = 'almond' and sub_name = 'butter' and abs_qty is NULL;
update results set abs_qty = round(normalised_qty::numeric * (95 / 236.0), 4) where name = 'almond' and sub_name = 'meal' and abs_qty is NULL;
update results set abs_qty = round(normalised_qty::numeric * (240 / 236.0), 4) where name = 'almond' and sub_name = 'milk' and abs_qty is NULL;
update results set abs_qty = round(normalised_qty::numeric * (14.5 / 14.8), 4) where name = 'cream' and sub_name = 'cheese' and abs_qty is NULL;
update results set abs_qty = round(normalised_qty::numeric * (8 / 14.8), 4) where name = 'macadamia' and sub_name = 'nuts' and abs_qty is NULL;
update results set abs_qty = round(normalised_qty::numeric * (112 / 236.0), 4) where name = 'mozzarella' and sub_name = 'cheese' and abs_qty is NULL;
update results set abs_qty = round(normalised_qty::numeric * (9 / 14.8), 4) where name = 'sesame' and sub_name = 'seeds' and abs_qty is NULL;

update results set abs_qty = round(normalised_qty::numeric * (5 / 14.8), 4) where name = 'butter' and abs_qty is NULL;
update results set abs_qty = round(normalised_qty::numeric * (5 / 14.8), 4) where sub_name = 'butter' and abs_qty is NULL;

update results set abs_qty = round(normalised_qty::numeric * (218 / 236.0), 4) where name = 'safflower' and abs_qty is NULL;
update results set abs_qty = round(normalised_qty::numeric * (218 / 236.0), 4) where name = 'oil' and abs_qty is NULL;
update results set abs_qty = round(normalised_qty::numeric * (218 / 236.0), 4) where sub_name = 'oil' and abs_qty is NULL;

update results set abs_qty = round(normalised_qty::numeric * (246 / 236.0), 4) where name = 'ricotta' and abs_qty is NULL;
update results set abs_qty = round(normalised_qty::numeric * (6.8 / 14.8), 4) where name = 'cheddar' and abs_qty is NULL;
update results set abs_qty = round(normalised_qty::numeric * (145 / 236.0), 4) where name = 'sugar' and abs_qty is NULL;
update results set abs_qty = round(normalised_qty::numeric * (5 / 14.8), 4) where name = 'pecorino' and abs_qty is NULL;

update results set abs_qty = round(normalised_qty::numeric * (15 / 14.8), 4) where name = 'ghee' and abs_qty is NULL;
update results set abs_qty = round(normalised_qty::numeric * (13.6 / 14.8), 4) where name = 'safflower' and abs_qty is NULL;
