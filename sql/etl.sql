insert into chunks (select chunk_id, document_id, confidence from etl group by chunk_id, document_id, confidence);
insert into terms (select id, chunk_id, term, tag, term_confidence as confidence from etl);

create index on terms (id);
create index on terms (chunk_id);

create materialized view valid_chunks as select * from chunks where confidence > 0.5;
create materialized view valid_terms as select * from terms where confidence > 0.85;

create index on valid_chunks (id);
create index on valid_terms (id);
create index on valid_terms (chunk_id);

create table fsanz_foods as
select name, category from (
  select name, 'fat' as category from fsanz where name like 'margarine' union
  select name, 'fat' as category from fsanz where name like 'butter' union
  select sub_name as name, 'fat' as category from fsanz where name like 'oil' union
  select name, 'fat' as category from fsanz where name like 'lard' union
  select name, 'fat' as category from fsanz where name like 'ghee' union
  select name, 'fat' as category from fsanz where name like 'suet' union
  select 'fetta' as name, 'cheese' as category union
  select 'feta' as name, 'cheese' as category union
  select 'cream cheese' as name, 'cheese' as category union
  select 'soft cheese' as name, 'cheese' as category union
  select regexp_replace(sub_name, 'creamed | style', '') as name, name as category from fsanz where name = 'cheese' and sub_name not like 'feta%' and sub_name not like 'cream' and sub_name not like 'soft'
) as t group by name, category;

create materialized view fsanz_terms as select * from terms t join fsanz_foods f on (t.term = f.name);
