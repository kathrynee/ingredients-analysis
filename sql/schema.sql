create table documents (
  id        serial primary key,
  year      int,
  file_name text
);
create unique index ON documents(file_name);

create table chunks (
  id integer primary key,
  document_id integer not null references documents(id),
  confidence double precision not null
);
create unique index on chunks (id, document_id);


create table terms (
  id integer,
  chunk_id integer not null references chunks(id),
  term text,
  tag text,
  confidence double precision
);

create table fsanz (
  id text primary key,
  name text,
  sub_name text,
  sub_sub_name text,
  optional_name text,
  description text,
  category text,
  sub_category text
);

-- ETL stuff.

create table etl (
  id                serial primary key,
  document_id       integer,
  chunk_id          integer,
  confidence        float,
  term              text,
  blah1             text,
  blah2             text,
  cap               text,
  parens            text,
  tag               text,
  term_confidence   float
);

alter table etl add constraint "chunks_document_id_fkey" foreign key (document_id) references documents(id);

-- Used by the import
create sequence etl_chunks_seq;

